#version 450

layout(location=0) out vec4 FragColor;

uniform sampler2D u_Texture;

in vec4 v_Color;
in vec2 v_OriXY;
in float v_Radius;

void main()
{
	/*vec4 newColor;
	float dist = sqrt(v_OriXY.x * v_OriXY.x + v_OriXY.y * v_OriXY.y);
	if(dist < v_Radius){
		newColor = v_Color;
		newColor.a = 1 - dist / v_Radius;
	}
	else{
		newColor = vec4(0);
	}
	FragColor = newColor;*/

	FragColor = texture(u_Texture, v_OriXY);
}
