#version 450

layout(location = 0) in vec3 a_Position;
layout(location = 1) in vec3 a_Vel;
layout(location = 2) in vec2 a_LifeTime;
layout(location = 3) in vec2 a_RatioAmp;
layout(location = 4) in float a_Value;
layout(location = 5) in vec4 a_Rgba;

// Varying --> fragment shader input
out vec4 v_Color;
out vec2 v_OriXY;
out float v_Radius;

uniform float u_Time;

const float PI = 3.141592;
const mat3 c_RP = mat3(0, -1, 0, 1, 0, 0, 0, 0, 0);
const vec3 c_Gravity = vec3(0, -1, 0);

void main()
{
	vec3 newPos = a_Position;
	v_OriXY = a_Position.xy;
	v_Radius = abs(a_Position.x);
	vec3 newVel = a_Vel.xyz;

	float ratio = a_RatioAmp.x;
	float amp = a_RatioAmp.y;
	float lifeTime = a_LifeTime.y;
	float newTime = u_Time - a_LifeTime.x;

	//newPos.x = cos(a_Value * 2 * PI) * 0.2f;
	//newPos.y = sin(a_Value * 2 * PI) * 0.2f;

	float newAlpha = 1.f;

	if(newTime > 0)
	{
		newPos.x += sin(a_Value * 2 * PI) * 0.3f;
		newPos.y += cos(a_Value * 2 * PI) * 0.3f;
		newTime = mod(newTime, lifeTime);
		newVel = newVel + c_Gravity * newTime;
		// 초기속도랑은 다른것 newVel은
		newPos = newPos + a_Vel * newTime + 0.5 * c_Gravity * newTime * newTime;	

		vec3 vSin = a_Vel*c_RP;
		//newPos = newPos + vSin * sin(newTime * PI * 2 * ratio) * amp;

		newAlpha = 1 - newTime / lifeTime;
	}
	
	else{
		newPos = vec3(100000, 100000, 100000);
	}

	gl_Position = vec4(newPos.xyz, 1);


	v_Color = vec4(a_Rgba.xyz, newAlpha);
	//v_Color = a_Rgba;
}
