#version 450

layout(location = 0) in vec4 a_Position;
layout(location = 1) in vec4 a_Color;

uniform float u_Time;

void main()
{
	gl_Position = vec4(a_Position.xyz, 1.0);
}
