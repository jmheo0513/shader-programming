#version 450

layout(location = 0) in vec3 a_Position;
layout(location = 1) in vec2 a_UV;

out vec4 v_Temp;

void main()
{
	gl_Position = vec4(a_Position.xyz, 1);
	v_Temp = vec4(a_Position.x + 0.5f, a_Position.y + 0.5f, 0, 1);

	//v_Temp = vec4(a_Position.xyz, 0);
	//gl_Position = vec4(a_Position.xyz, 1);
}
