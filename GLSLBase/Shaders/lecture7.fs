#version 450

layout(location=0) out vec4 FragColor;

in vec4 v_Temp;

uniform float u_Time;
uniform vec2 u_Points[5];


void main()
{
	vec2 newUV = vec2(v_Temp.xy) - vec2(0.5f, 0.5f); // -0.5 ~ 0.5

	float pointGrey = 0;

	for(int i=0; i<2; ++i){
		vec2 newPoint = u_Points[i];
		vec2 newVec = newPoint - newUV;
		float distance = length(newVec);
		if(distance < 0.1){
			pointGrey += 0.5 * (distance / 0.1);
		}
	}

	float distance = length(newUV);
	float newTime = fract(u_Time);
	//float distance = sqrt(newUV.x*newUV.x + newUV.y*newUV.y);
	//float grey = sin(distance * 3.141592 * 4 * 5);

	if(distance < newTime + 0.05 && distance > newTime - 0.05){
		pointGrey = 1;
	}

	/*if(distance < 0.5f){
		FragColor = vec4(1);
	}
	else{
		FragColor = vec4(0);
	}*/

	
	FragColor = vec4(pointGrey);
	//FragColor = v_Temp;
}
